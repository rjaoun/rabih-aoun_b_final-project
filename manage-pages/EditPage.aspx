﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="manage_pages.EditPage" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h3 runat="server" id="page_Name">Edit Page</h3>

    <asp:SqlDataSource runat="server" id="edit_page"
        ConnectionString="<%$ ConnectionStrings:finalproject_sql_con %>">

    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" id="page_select"
        ConnectionString="<%$ ConnectionStrings:finalproject_sql_con %>">
    </asp:SqlDataSource>  
    <div class="inputrow">
        <label>Page Name:</label>
        <asp:TextBox ID="page_title" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="page_title"
            ErrorMessage="Enter a page name" ></asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <label>Page Content:</label>
        <asp:TextBox ID="page_content" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="page_content"
            ErrorMessage="Enter a page content" ></asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <uctrl:pick_author runat="server" ID="Pick_Author" />
    </div>

    <asp:Button Text="Edit Page" runat="server" OnClick="Edit_Page" />

    <div runat="server" id="debug" class="querybox"></div>

</asp:Content>
