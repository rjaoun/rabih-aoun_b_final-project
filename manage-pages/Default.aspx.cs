﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace manage_pages
{
    public  partial  class _Default : System.Web.UI.Page
        
    {
        // I have tried to do the extra feature that allows the user to select a page theme
        // by having two skins one called Dark, and other called Light, I have added profile
        // tag in the web.config with property of the theme. and in the Default.aspx I have addded
        // a dropdownlist to select which theme. However, in the Default.aspx.cs I am having problem
        // to assign the selected Theme to the profile. I got some help from 
        // https://weblogs.asp.net/scottgu/Recipe_3A00_-Dynamic-Site-Layout-and-Style-Personalization-with-ASP.NET--
        // which is in ASP.net, I am pretty sure there is another way to get the Theme selection to assign to the profile
        // in C#. 
        /*
        public void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                ddlThemePreference.SelectedValue = profile.ThemePreference;
            }
        }

        public void UpdateBtn_Click(object sender, EventArgs e)
        {
            Profile.ThemePreference = ddlThemePreference.SelectedValue;

            Response.Redirect("default.aspx");
        }*/
        /*protected void PagePreInit(object sender, EventArgs e)
        {
            if(Request["theme"] != null)
            {
                switch (Request["theme"])
                {
                    case "Dark":
                        ProfileParameter.ThemePreference = "Dark";
                        break;
                    case "Light":
                        Profile.userTheme = "Light";
                        break;
                }
            }
            Theme = Profile.userTheme;
        }
        */
    }
}