﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="page_view.aspx.cs" Inherits="manage_pages.page_view" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h1 id="page_name" runat="server"></h1>

    <asp:SqlDataSource runat="server"
        id="page_select"
        ConnectionString="<%$ ConnectionStrings:finalproject_sql_con %>">
    </asp:SqlDataSource>

    <!--<asp:DataGrid ID="page_list" runat="server">
    </asp:DataGrid> -->
    <div id="page_query" runat="server" class="querybox">
    </div>

    <h3 id="page_content" runat="server"></h3>
    <asp:SqlDataSource 
        runat="server"
        id="del_page"
        ConnectionString="<%$ ConnectionStrings:finalproject_sql_con %>">
    </asp:SqlDataSource>

    <h3 id="page_author" runat="server"></h3>
    <!-- I got this part from Christine CRUD example, to create the Edit link and Delete Button-->
    <div class="inputrow">
        <a href="EditPage.aspx?pageid=<%Response.Write(this.pageid);%>">Edit</a>
    </div>
    <div class="inputrow">
        <asp:Button runat="server" id="del_page_btn"
            OnClick="DelPage"
            OnClientClick="if(!confirm('Are you sure you want to Delete this Page?')) return false;"
            Text="Delete" />
    </div>
    <!--<div id="del_debug" class="querybox" runat="server"></div>-->

</asp:Content>