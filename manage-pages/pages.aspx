﻿<%@ Page Title="Pages" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="pages.aspx.cs" Inherits="manage_pages.pages" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Manage Pages</h1>

    <!-- This a is to create a new page -->
    <div class="inputrow">
        <a href="NewPage.aspx">New Page</a>
    </div>

    <!-- textbox and search button to search for a specific page
        This part is taken from Christine's CRUD example-->
    <asp:TextBox runat="server" ID="page_key"></asp:TextBox>
    <asp:Button runat="server" Text="Search" OnClick="Search_Pages"    />
    <!--I am using this div to display error message when the search value is empty instead of showing the sql query -->
    <div class="querybox" id="page_query" runat="server">
    </div> 

    <!-- To get the Data from the Pages table -->
    <asp:sqldatasource runat="server" ID="page_select" 
        connectionString ="<%$connectionstrings:finalproject_sql_con %>"></asp:sqldatasource>
    <!-- To show the Data of the Pages table-->
    <asp:DataGrid runat="server" ID="page_list" AutoGenerateColumns="false" ></asp:DataGrid>
    


    
</asp:Content>
