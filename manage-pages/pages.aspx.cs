﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace manage_pages
{
    public partial class pages : System.Web.UI.Page
    {
        private string sqlline = " SELECT pageid, pagetitle, pagecontent,publishdate, authorname " +
                                 " FROM Pages left join authors on authors.authorid = pages.authorid ";

        protected void Page_Load(object sender, EventArgs e)
        {
            page_select.SelectCommand = sqlline;
            //This line to show the sql query
            //page_query.InnerHtml = sqlline;
            

            page_list.DataSource = pages_Manual_Bind(page_select);
           
            BoundColumn p_id = new BoundColumn();
            p_id.DataField = "pageid";
            p_id.Visible = false;
            p_id.HeaderText = "PAGE ID";
            page_list.Columns.Add(p_id);

            BoundColumn p_title = new BoundColumn();
            p_title.DataField = "pagetitle";
            p_title.HeaderText = "PAGE TITLE";
            page_list.Columns.Add(p_title);

            BoundColumn a_name = new BoundColumn();
            a_name.DataField = "authorname";
            a_name.HeaderText = "Author Name";
            page_list.Columns.Add(a_name);
            
            BoundColumn p_content = new BoundColumn();
            p_content.DataField = "pagecontent";
            p_content.HeaderText = "PAGE CONTENT";
            page_list.Columns.Add(p_content);

            BoundColumn p_date = new BoundColumn();
            p_date.DataField = "publishdate";
            p_date.HeaderText = "DATE PUBLISHED";
            page_list.Columns.Add(p_date);
            
            page_list.DataBind();
        }
        
        protected DataView pages_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;

            DataView myview;

            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            
            // foreach to make each page title a link connected to the page id.
            foreach (DataRow row in mytbl.Rows)
            {
                row["pagetitle"] =
                    "<a href=\"page_view.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["pagetitle"]
                    + "</a>";

            }
            //mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;
            return myview;
        }
        protected void Search_Pages(object sender, EventArgs e)
        {
            string newquery = sqlline + " WHERE  (1=1) ";
            string keySearch = page_key.Text;
            // in the search box, the query is pagetitle LIKE "keySearch%"; 
            // that will make sure that the started letter should match one of the 
            // starting letter of any of the page we have, otherwise it will not be found
            // I could make it work to search for any letter in any order by adding % to the beginning
            // of the search, but this way it makes more sense to me.
            if (keySearch != "")
            {
                newquery += " AND pagetitle LIKE '" + keySearch + "%'";
            }
            else if (keySearch == "")
            {
                page_query.InnerHtml = "No Data is selected";
            }

            page_select.SelectCommand = newquery;
            //page_query.InnerHtml = newquery;

            page_list.DataSource = pages_Manual_Bind(page_select);

            page_list.DataBind();
        }
    }
}