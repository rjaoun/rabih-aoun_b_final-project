﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace manage_pages.usercontrols
{
    public partial class PickAuhtor : System.Web.UI.UserControl
    {
        private string subquery = "SELECT authorid, authorname from Authors";

        private int selected_id;
        public int _selected_id
        {
            get { return selected_id; }
            set { selected_id = value;  }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            authors_list_pick.SelectCommand = subquery;

            AuthorPick_Manual_Bind(authors_list_pick, "author_pick");
        }
        void AuthorPick_Manual_Bind(SqlDataSource src, string ddl_id)
        {
            DropDownList authorlist = (DropDownList)FindControl(ddl_id);

            DataView myview = (DataView)src.Select(DataSourceSelectArguments.Empty);

            foreach (DataRowView row in myview)
            {
                ListItem author_item = new ListItem();
                author_item.Text = row["authorname"].ToString();
                author_item.Value = row["authorid"].ToString();
                authorlist.Items.Add(author_item);
            }
        }
        protected void Assign_AuthorID(object sender, EventArgs e)
        {
            _selected_id = int.Parse(author_pick.SelectedValue);
        }
    }
}