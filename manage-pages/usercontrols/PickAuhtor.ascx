﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PickAuhtor.ascx.cs" Inherits="manage_pages.usercontrols.PickAuhtor" %>

<label>Pick Author</label>

<asp:SqlDataSource 
    runat="server" 
    ID="authors_list_pick"
    ConnectionString="<%$ ConnectionStrings:finalproject_sql_con %>">
</asp:SqlDataSource>

<asp:DropDownList 
    runat="server" 
    ID="author_pick"
    OnTextChanged="Assign_AuthorID">

</asp:DropDownList>